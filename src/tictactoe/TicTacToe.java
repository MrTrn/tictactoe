/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;

import java.util.Scanner;

/**
 *
 * @author rene
 */
public class TicTacToe {
    final static int BOARDSIZE=3; // The size of board is (BOARDSIZE x BOARDSIZE)
    static String[][] theBoard = new String[BOARDSIZE][BOARDSIZE]; // The board 
    static String xOrO = "X"; /* keeps track of what player, X or O
     * Player X always starts, thats the rule.
     */
    
   public static void playTicTacToe() {
       int  i=1; // counter for how many rounds
        resetBoard(); // fills the board with numbers
        printHelp(); // shows the user help
        
        do {
            // playGame() handles input and game stuff
            printOut(); // prints the board
        } while ((playGame() == true)  && (i<=(BOARDSIZE*BOARDSIZE)));
        // end of game:
        System.out.println("\nThank you for playing! :)");
   }
      /** playGame()
     * Handles the main actions for the game
     * @return boolean true as long noone wins or ends the game
     */
    private static boolean playGame() {
        
        int userPlacement,r,c; // c for columns, r for rows
        
        System.out.println("You are player: " + xOrO + "\nPlease enter your move: ");
        
        /* Get and validate input from user
         * If validating fails, user gets error and a new try */
        do {
            userPlacement = validateIntValueFromUser(1,(BOARDSIZE*BOARDSIZE)); // from user
            if (userPlacement == -2) { return false; } // user typed "end", end game
            else if (userPlacement<0) {
                System.out.println("Your move must be a number between 1 and " + (BOARDSIZE*BOARDSIZE));
            }
            
        } while (userPlacement<0);
        if (userPlacement == 0) { r=0; c=0; }
        /* Java didnt like looking up [-1/3]*/
        else { 
            r = (userPlacement-1)/BOARDSIZE;
            c = (userPlacement-1)%BOARDSIZE;
        }
        if ((theBoard[r][c] == "X") || (theBoard[r][c] == "O")) {
            // there is a mark in that place
            System.out.println("\nThis spot is taken!");
        }
        else {

            theBoard[r][c] = xOrO; /* add X or O */
            if (checkBoard() == true) { return false; } /* xOrO won! return 
             * false and end game */ 
            /* change player */
            if (xOrO == "X") { xOrO ="O"; }
            else {
                xOrO = "X";
            }
        }
        return true; // keep playing
    }
       
    /** void printHelp()
     * Prints the help lines
     */
    public static void printHelp() {
        System.out.println("* How to play:\n* Enter the number where you want your marker.");
        System.out.println("* You win when you have " + BOARDSIZE + " marks in a row.");
        System.out.println("* At any time type \"end\" to end the game\n");
        System.out.println("* or type \"help\" to show this again\n");
    }
    
    /** void printOut()
     * Prints out the board
     */
    public static void printOut() {
        int c, r, count=1, cDots; // c for columns, r for rows
        for (r=1; r<=BOARDSIZE ;r++) {
            System.out.print(""+ theBoard[(r-1)][0]);
            for (c=1; c<=(BOARDSIZE-1);c++) {
                count++;
                /* Design: try to algin the numbers of boards larger than 3x3*/
                if ((count<=9) && (BOARDSIZE>3)) { System.out.print("  | " + theBoard[(r-1)][c] ); }
                else { System.out.print(" | " + theBoard[(r-1)][c] ); }                
            }
            if (r<BOARDSIZE) { 
                /* Design: Dont want a line under the last row  
                 * and; dont want too short a line*/
                System.out.print("\n-");
                for (cDots=1;cDots<BOARDSIZE;cDots++){
                    /*aprox 5 dots per number*/
                    if (BOARDSIZE>3) { System.out.print("------");  }
                    else { System.out.print("-----"); }
                }
                System.out.print("\n");
               } 
            count++;
        }
        System.out.println("\n");
    }
    
    /** void resetBoard()
     * Fills theBoard[][] with numbers
     */
    public static void resetBoard() {
        int c, r, counter=0; // c for columns, r for rows
        for (r=0; r<=BOARDSIZE-1 ;r++) {
            for (c=0; c<=(BOARDSIZE-1);c++) {
                counter++;
                theBoard[r][c] = String.valueOf(counter);               
            }
        }
    }
    
    /**checkBoard()
     * Checks the board in all different cases 
     * @requires printOut()
     * @return boolean true if player has a winning match
     * also prints out the who won, and in what way they won - just for kicks
     */
    public static boolean checkBoard() {
        if (checkRows()) {
            printOut(); // last printout of board
            System.out.println("* Congrats, "+ xOrO + " won!");
            System.out.println("* Winner type: " + "horizontal");
            return true;
        }
         else if (checkColums()) {
            printOut(); // last printout of board
            System.out.println("* Congrats, "+ xOrO + " won!");
            System.out.println("* Winner type: " + "vertical");
            return true;
        }
       else if (checkDiagLR()) {
            printOut(); // last printout of board
            System.out.println("* Congrats, "+ xOrO + " won!");
            System.out.println("* Winner type: " + "diagonal upper left to lower right");
            return true;
        }
        else if (checkDiagRL()) {
            printOut(); // last printout of board
            System.out.println("* Congrats, "+ xOrO + " won!");
            System.out.println("* Winner type: " + "diagonal upper right to lower left");
            return true;
        }
        return false;
    }
    /** checkColums()
     * Checks the board colum wise
     * [|][|][|]
     * [|][|][|]
     * [|][|][|]
     * @return boolean true if player wins
     */
     public static boolean checkColums() {
            // NTL: se etter matrisesøk.. 
        int c=0, r=0, d=(BOARDSIZE-1); // r for columns, c for rows, d for BOARDSIZE-1
        int counter; // counts how many alike
        boolean whileCheck = false; // "while" didnt like mixing boolean and int 

          do {
                counter=0; //starting on new row, reset counter 
                for (c=0; c <= (BOARDSIZE-1);c++) {
                    if (theBoard[c][r] == xOrO) {
                       counter++;
                    }             
                }
                if (counter == BOARDSIZE) {
                    whileCheck = true;
                    return true;
                }
                r++;
          } while ((whileCheck == false) && (r<=d));
        return false;
    }

     /** checkRows()
     * Checks the board row wise
     * [-][-][-]
     * [-][-][-]
     * [-][-][-]
     * @return boolean true if player wins
     */
    private static boolean checkRows() {
         int c=0, r=0, d=(BOARDSIZE-1); // c for columns, r for rows, d for BOARDSIZE-1
        int counter; // counts how many alike
        boolean whileCheck = false; // 

          do {
                counter=0; //starting on new row, reset counter 
                for (c=0; c <= (BOARDSIZE-1);c++) {
                    /* System.out.println("["+r+"]["+c+"]" + theBoard[r][c] + 
                       " == " + xOrO + " c: " + counter); // debug */
                    if (theBoard[r][c] == xOrO) {
                       counter++;
                    }             
                }
                if (counter == BOARDSIZE) {
                    whileCheck = true;
                /*  System.out.println("whileCheck == " + String.valueOf(whileCheck)); //degbug */
                    
                    return true;
                }
                r++;
          } while ((whileCheck == false) && (r<=d));
        return false;
    }
      
     /** checkDiagLR()
     * Checks the board from upper Left to lower Right corner
     * [\][ ][ ]
     * [ ][\][ ]
     * [ ][ ][\]
     * @return boolean true if player wins
     */
      private static boolean checkDiagLR() {
         int c=0, r=0, d=(BOARDSIZE-1); // c for columns, r for rows, d for BOARDSIZE-1
        int counter=0; // counts how many alike
        boolean whileCheck = false; 

          do {
               // counter=0; //starting on new row, reset counter 
               // for (c=0; c <= (BOARDSIZE-1);c++) {
                   /*  System.out.println("["+r+"]["+r+"]" + theBoard[r][c] + 
                       " == " + xOrO + " c: " + counter); // debug */
                    if (theBoard[r][r] == xOrO) {
                       counter++;
                    }             
               // }
                if (counter == BOARDSIZE) {
                    whileCheck = true;
                 /* System.out.println("whileCheck == " + String.valueOf(whileCheck)); //degbug */
                    
                    return true;
                }
                r++;
          } while ((whileCheck == false) && (r <= d));
        return false;
    }
       
     /** checkDiagRL()
     * Checks the board from upper Right to lower Left corner
     * [ ][ ][/]
     * [ ][/][ ]
     * [/][ ][ ]
     * @return boolean true if player wins
     */
      private static boolean checkDiagRL() {
         int c=(BOARDSIZE-1), d=(BOARDSIZE-1), r=0; 
        int counter=0; // counts how many alike
        boolean whileCheck = false;  

          do {
              
              /*System.out.println("* ["+r+"]["+c+"]" + theBoard[r][c] + 
                       " == " + xOrO + " c: " + counter); // debug */
                    if (theBoard[r][c] == xOrO) {
                       counter++;
                    }    
              
                    if (counter == BOARDSIZE) {
                    whileCheck = true;
                 /* System.out.println("whileCheck == " + 
                        String.valueOf(whileCheck)); //degbug */
                    
                    return true;
                }
                r++;c--;
          } while ((whileCheck == false) && (r <= d));
        return false;
    }
      
    /** validateIntValueFromUser(int minValue, int maxValue)
     * Get user input and validate as integer or as String with value "end"
     * @param int >=0, int >0
     * @return int >=0 on valid input, -2 on String "end", -1 if not int
     * 
     */
    public static int validateIntValueFromUser(int minValue, int maxValue) {

        int value;
        Scanner key = new Scanner(System.in); // create scanner obj for keyboard input
        // check to see if input is a integer
        if (key.hasNextInt()){
            // input is in fact a integer
            value = key.nextInt();
            // Value must be between minValue-maxValue
            if (value>=minValue && value <=maxValue)
                return value;
        }
        /* Other cases:
         * written commands availble anytime during the game
         */
        else if (key.hasNext("end")) {
            return -2; // return error code -2
        }
        else if (key.hasNext("help")) {
            return -3; // return error code -3
        }
        /* input is not an integer OR did not meet the requirements
         * return error value
         */
            return -1;
    }   
}
