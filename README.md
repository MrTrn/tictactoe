# TicTacToe console game, CSCI 2912 fall 2011#
School assignment from Computer Science II - CSCI 2912 at Hawaii Pacific University, fall 2011.

A simple console version of the TicTacToe game in java.
The board size is scaleable with a static variable, so any reasonable board size should work. 

## Techologies / topics ##
* Double arrays
* Validate user input
* Who's the winner?

Find a friend and get ready for a 3x3 game of TicTacToe. 
The winner will be announced ;)

**Note:** *This is an old code from an early stage in my edu. It works, but it's not a good example of Java programming. Don't judge me ;)*